# aws-landingzone

## Summary

This is the repository for the core components that need to be built first in creating a landing zone inside a new AWS account.

## Components

Listed in order of dependencies (i.e. created top down).

### terraform-state

Creates an S3 bucket and DynamoDB table to be used in terraform for managing state data.

### core-bixbots

Deploys core components for the `bixbots` VPC.

NAT gateways that also serve as jump (aka bastion) hosts:
- us-east-1a-nat.bixbots.cloud
- us-east-1b-nat.bixbots.cloud
- us-east-1c-nat.bixbots.cloud
