module "state" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/terraform-state.git?ref=v1"

  lock_table_name   = "terraform-lock"
  state_bucket_name = "bixbots-terraform-state"

  tags = {
    "TerraformModule" = "core/aws-landingzone/terraform-state"
  }
}
